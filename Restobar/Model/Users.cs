﻿using System;
using System.Collections.Generic;

namespace Restobar.Model
{
    public class Users
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Telefono { get; set; }
        public IEnumerable<string> Role { get; set; }
        
    }
}
