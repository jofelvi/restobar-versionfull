﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Restobar.Interfaces;
using Restobar.Model;
using Restobar.Services;


namespace Restobar.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class Autentificacion : ControllerBase
    {
        IdecodeTokenServices _idecodeTokenServices;

        //private readonly DecodeTokenServices _decodeTokenServices;

        public Autentificacion(IdecodeTokenServices idecodeTokenServices)
        {
            _idecodeTokenServices = idecodeTokenServices;
        }
   
        
        [Authorize]
        [HttpGet("prueba")]
        public async Task<ActionResult<Users>> Get()
        {

            var accessToken = await HttpContext.GetTokenAsync("access_token");


            var decodeTokenServices = new DecodeTokenServices();
            
            object decodetoken = _idecodeTokenServices.DecodeTokenCreateUser(accessToken);
            //object decodetoken = decodeTokenServices.DecodeTokenCreateUser(accessToken);


            return  Ok(decodetoken);




        }

       
        }
}

