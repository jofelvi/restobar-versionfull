using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Restobar.Model;
using webapi_csharp.Modelos;
using webapi_csharp.Repositorios;

namespace Restobar.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContacController : ControllerBase
    {
     

        public ContacController( )
        {
           
        }

        // GET: api/Contac
        [HttpGet]
        public IActionResult Get()
        {
            RPClientes rpCli = new RPClientes();
            return Ok(rpCli.ObtenerClientes());
        }

        // GET: api/Contac/

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            RPClientes rpCli = new RPClientes();

            var cliRet = rpCli.ObtenerCliente(id);

            if (cliRet == null)
            {
                var nf = NotFound("El cliente " + id.ToString() + " no existe.");
                return nf;
            }

            return Ok(cliRet);
        }

        [HttpPost("agregar")]
        public IActionResult AgregarCliente(Cliente nuevoCliente)
        {
            RPClientes rpCli = new RPClientes();
            rpCli.Agregar(nuevoCliente);
            return CreatedAtAction(nameof(AgregarCliente), nuevoCliente);
        }
    }
}
