﻿using System;
namespace Restobar.Interfaces
{
    public interface IdecodeTokenServices
    {
        object DecodeTokenCreateUser(string accessToken);
    }
}
