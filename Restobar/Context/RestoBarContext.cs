﻿using System;
using Microsoft.EntityFrameworkCore;
using Restobar.Model;
using webapi_csharp.Modelos;

namespace Restobar.Context
{
    public class RestoBarContext: DbContext 
    {
        public RestoBarContext(DbContextOptions<RestoBarContext> options):base(options)
        {
        }


        public DbSet<Cliente> ClienteItems { get; set; }
        
        public DbSet<Contact> ContactoItems { get; set; }

        public DbSet<Users> UsersItems { get; set; }


    }
}
