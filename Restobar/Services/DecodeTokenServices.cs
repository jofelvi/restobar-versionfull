﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

using Restobar.Context;
using Restobar.Interfaces;
using Restobar.Model;

namespace Restobar.Services
{
    public class DecodeTokenServices: IdecodeTokenServices
    {
        //private readonly RestoBarContext _context; 


        public DecodeTokenServices()
        {
            //_context = context;
           
        }

        public object DecodeTokenCreateUser(string accessToken)
        {


            var user = new Users();
            List<string> array_list = new List<string>();

            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(accessToken);
            var tokenS = handler.ReadToken(accessToken) as JwtSecurityToken;
            var role = tokenS.Claims.First(claim => claim.Type == "role").Value;
            user.Nombre = tokenS.Claims.First(claim => claim.Type == "nombre").Value;
            user.Apellido = tokenS.Claims.First(claim => claim.Type == "apellido").Value;
            user.Telefono = tokenS.Claims.First(claim => claim.Type == "telefono").Value;
            user.Email = tokenS.Claims.First(claim => claim.Type == "email").Value;
            user.Password = tokenS.Claims.First(claim => claim.Type == "password").Value;
            array_list.Add(role);
            user.Role = array_list;

            
            if (user.Nombre == null || user.Apellido == null || user.Telefono == null || user.Email == null || user.Password == null || user.Nombre == "" || user.Apellido == "" || user.Telefono == "" || user.Email == "" || user.Password == "")
            {

                 return "Falta algun valor en el json";
            }
            else
            {
                return user;

            }
           
        }
    }
}
