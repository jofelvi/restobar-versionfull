﻿using System.Collections.Generic;
using System.Linq;
using Restobar.Model;
using webapi_csharp.Modelos;

namespace webapi_csharp.Repositorios
{
    public class RPCcontacto
    {
        public static List<Contact> _listaContact = new List<Contact>()
        {
            new Contact() { Id = 1, Name = "Cliente 1" , Email = "Email 1" },
            new Contact() { Id = 2, Name = "Cliente 2" , Email = "Email 2" },
            new Contact() { Id = 3, Name = "Cliente 3" , Email = "Email 3" }
        };

        public IEnumerable<Contact> ObtenerClientes()
        {
            return _listaContact;
        }

        public Contact ObtenerCliente(int id)
        {
            var Contact = _listaContact.Where(cli => cli.Id == id);

            return Contact.FirstOrDefault();
        }

        public void Agregar(Contact nuevoContacto)
        {
            _listaContact.Add(nuevoContacto);
        }
    }
}